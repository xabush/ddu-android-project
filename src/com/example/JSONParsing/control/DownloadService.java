package com.example.JSONParsing.control;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import com.example.JSONParsing.model.Course;
import com.example.JSONParsing.model.Student;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

/**
 * Created by Xabush on 8/19/2015 15:58.
 * Project: JSONParsing
 */
public class DownloadService extends IntentService {

    //Status report flags;
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    //JSON Node tags
    private static final String ID_TAG = "id";
    private static final String SEMESTER_TAG = "semester";
    private static final String CID_TAG = "CourseCode";
    private static final String COURSES_TAG = "courses";
    private static final String CNAME_TAG = "CourseTitle";
    private static final String CREDIT_TAG = "CreditHour";
    private static final String GRADE_TAG = "Grade";
    private static final String SGPA_TAG = "semester_gpa";
    private static final String PSGPA_TAG = "psemester_gpa";
    private static final String TGPA_TAG = "total_gpa";

    private static final String TAG = "DownloadService";

    private static HttpRequest htttpRequest;

    private Student student;

    public DownloadService(){
        super(DownloadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Log.d(TAG, "Service Started!");

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        student = intent.getParcelableExtra("student");
        Log.d(TAG, "Student with id: " + student.getmId() + " received!");
        Bundle bundle = new Bundle();

        if (!TextUtils.isEmpty(url)){
            /* Update UI: Download Service is Running */
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);

            try {
                 downloadData(url);
                 Log.d(TAG, "Finished downloading data. About to Send...");
                //Sending result back to the calling activity
                bundle.putParcelable("result", student);
                receiver.send(STATUS_FINISHED, bundle);
                Log.d(TAG, "Data sent!");
            } catch(Exception e)
            {

                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);
                Log.e(TAG, e.toString());
                e.printStackTrace();
            }
        }

        Log.d(TAG, "Service Stopping!");
        this.stopSelf();
    }

    private void downloadData(String requestUrl) throws IOException, JSONException
    {

        Log.d(TAG, "Url: " + requestUrl);
          //TODO Uncomment the following line when the server starts working
      /*  htttpRequest = new HttpRequest(requestUrl);
        JSONObject jsonObj = htttpRequest.prepare().sendAndReadJSON();*/

        JSONObject jsonObj = getLocalFile();
        String id = jsonObj.optString(ID_TAG);
        if (student.getmId().equals(id))
        {
            String semester = jsonObj.optString(SEMESTER_TAG);

            //parse the courses
            JSONArray courseArray = jsonObj.optJSONArray(COURSES_TAG);
            for (int i = 0; i < courseArray.length(); i++)
            {
                JSONObject courseObj = courseArray.getJSONObject(i);
                String course_id = courseObj.getString(CID_TAG);
                String course_name = courseObj.getString(CNAME_TAG);
                int course_credit = courseObj.getInt(CREDIT_TAG);
                String course_grade = courseObj.getString(GRADE_TAG);

                Course c = new Course(course_id, course_name,course_grade,course_credit,semester);
                student.getCourses().add(c);
            }


            /*
            student.setTotal_gpa((float)jsonObj.optDouble(TGPA_TAG));
            student.setPrev_sem_gpa((float)jsonObj.optDouble(PSGPA_TAG));
            student.setSem_gpa((float)jsonObj.optDouble(SGPA_TAG));
            */

            student.setTotal_gpa(0);
            student.setPrev_sem_gpa(0);
            student.setSem_gpa(0);
        }

    }

    /**
     * This method is created for testing purpose only
     */

    private JSONObject getLocalFile() throws IOException, JSONException
    {
        return new JSONObject(sendAndReadString());
    }

    /**
     * This method is created for testing purpose only
     */

    private String sendAndReadString() throws IOException{
//		BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));   Commented for testing purpose
        InputStream in = getClass().getResourceAsStream("actual_format.json");
        BufferedReader br=new BufferedReader(new InputStreamReader(in));
        String line,response="";
        while ((line=br.readLine()) != null)response+=line;
        br.close();
        return response;
    }

}
