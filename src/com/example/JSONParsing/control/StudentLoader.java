package com.example.JSONParsing.control;

import android.content.Context;
import com.example.JSONParsing.model.Student;
import com.example.JSONParsing.model.StudentDbAdapter;

/**
 * Created by Xabush on 9/7/2015 10:59.
 * Project: JSONParsing
 */
//Student Loader Class
public class StudentLoader extends DataLoader<Student> {

    private String mSid;
    private StudentDbAdapter dbAdapter;


    public StudentLoader(Context context, String sid) {
        super(context);
        mSid = sid;
        dbAdapter = new StudentDbAdapter(context);
    }


    @Override
    public Student loadInBackground() {
        return dbAdapter.queryStudent(mSid);
    }
}
