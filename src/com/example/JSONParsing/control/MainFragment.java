package com.example.JSONParsing.control;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;
import com.example.JSONParsing.R;
import com.example.JSONParsing.model.Student;
import com.example.JSONParsing.model.StudentDbAdapter;

/**
 * Created by Xabush on 8/24/2015 10:59.
 * Project: JSONParsing
 */
public class MainFragment extends Fragment implements DownloadResultReceiver.Receiver {

    private String mId;
    private static final String TAG = "MainFragment";
    private static final String EXTRA_ID = "com.example.JSONParsing.control";
    private static final String URL = "http://192.168.56.1:8000/webservices/studentinfo/?key=KoxO8DMgwq4l&id=";
    private static final String INPUT_DIALOG = "ID_Input_Dialog";

    private Student mStudent;
    private DownloadResultReceiver receiver;
    private StudentDbAdapter studentDbAdapter;

    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        mId = getArguments().getString(EXTRA_ID);
        studentDbAdapter = new StudentDbAdapter(getActivity());
        Log.d(TAG, "ID: " + mId);
        mStudent = new Student(mId); //Initialize the mStudent object with the given id
        receiver = new DownloadResultReceiver(new Handler());
        receiver.setReceiver(this);

        //Fetch the JSON data from the server using the mStudent id
        fetchData();
    }


    public void fetchData()
    {
        String url = URL + mId;
        Intent intent = new Intent(Intent.ACTION_SYNC,null,getActivity(), DownloadService.class);
        intent.putExtra("url", url);
        intent.putExtra("receiver", receiver);
        intent.putExtra("student", mStudent);

        getActivity().startService(intent);

    }



    public static MainFragment newInstance(String id)
    {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_ID, id);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case DownloadService.STATUS_RUNNING:
                showProgressDialog(true);
                break;
            case DownloadService.STATUS_FINISHED:
                showProgressDialog(false);
                mStudent = resultData.getParcelable("result");
                //insert the student to the database;
                studentDbAdapter.insertStudent(mStudent);
                Log.d(TAG, "Data Received successfully!");
                FragmentManager fm = getActivity().getSupportFragmentManager();
                GradeListFragment gradeFragment = GradeListFragment.newInstance(mStudent, mId);
                fm.beginTransaction().replace(R.id.fragmentContainer, gradeFragment).commit();
                break;
            case DownloadService.STATUS_ERROR:
                /* Handle the error */
                showProgressDialog(false);
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                break;


        }
    }

    private void showProgressDialog(boolean show)
    {
        if (show)
        {
            if (pDialog == null)
            {
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();

            }
            else{
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        else
            pDialog.cancel();
    }



}
