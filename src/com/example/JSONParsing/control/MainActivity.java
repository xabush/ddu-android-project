package com.example.JSONParsing.control;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.example.JSONParsing.R;
import com.example.JSONParsing.model.Student;

/**
 * Created by Xabush on 8/24/2015 11:32.
 * Project: JSONParsing
 */
public class MainActivity extends FragmentActivity {

    private static final String INPUT_DIALOG = "ID_Input_Dialog";
    private static final String PREFS_TAG = "students";
    private static SharedPreferences mPrefs;
    private String mId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        mPrefs = getSharedPreferences(PREFS_TAG, MODE_PRIVATE);
        mId = mPrefs.getString("sid", null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FragmentManager fm = getSupportFragmentManager();
        if (mId == null)
        {

            InputDialogFragment fragment = InputDialogFragment.newInstance("Please Input Your ID");
            fragment.show(fm, INPUT_DIALOG);
        }
        else
        {
            Student student = new Student();
            GradeListFragment fragment = GradeListFragment.newInstance(student, mId);
            fm.beginTransaction().add(fragment, null).commit();
        }

    }




}

