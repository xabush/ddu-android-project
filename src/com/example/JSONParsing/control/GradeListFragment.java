package com.example.JSONParsing.control;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.JSONParsing.R;
import com.example.JSONParsing.model.Course;
import com.example.JSONParsing.model.Student;
import com.example.JSONParsing.model.StudentDbAdapter;
import com.example.JSONParsing.model.StudentVm;

import java.util.ArrayList;

/**
 * Created by Xabush on 8/25/2015 17:04.
 * Project: JSONParsing
 */
public class GradeListFragment extends ListFragment {

    public static final String EXTA_STUDENT = "control.GradeListFragment.Student";
    public static final String EXTRA_ID = "control.GradeListFragment.Id";
    private static final String TAG = "GradeList";
    private String mId;
    private Student mStudent;
    private ProgressDialog pDialog;
    private StudentDbAdapter dbAdapter;
    private static final String PREFS_TAG = "students";
    private static SharedPreferences mPrefs;
    private long _id;


    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        //Initialize studentVm objects for the ListAdapter
        dbAdapter = new StudentDbAdapter(getActivity());
        mPrefs = getActivity().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        _id = mPrefs.getLong("_id", -1);
        mStudent = getArguments().getParcelable(EXTA_STUDENT);
        mId = getArguments().getString(EXTRA_ID);

    //              mStudent = dbAdapter.queryStudent(mId);
         getActivity().getSupportLoaderManager().initLoader(0, null, new StudentLoaderCallbacks());
         getActivity().getSupportLoaderManager().initLoader(1, null, new CoursesLoaderCallbacks());


        updateUI(mStudent);

    }



    /**
     * Our custom GradeList Adapter that uses StudentVm objects
     */

    private class GradeListAdapter extends ArrayAdapter<StudentVm>
    {
        public GradeListAdapter(ArrayList<StudentVm> studentVms)
        {
            super(getActivity(),0,studentVms);
        }

        @Override
        public View getView(int position, View cv, ViewGroup vg){
            if (cv == null)
            {
                cv = getActivity().getLayoutInflater().inflate(R.layout.list_item,null);
            }

            StudentVm studentVm = getItem(position);

            //Wire the views to the model object
            TextView courseTxtV = (TextView)cv.findViewById(R.id.course_name);
            TextView gradeTxtV = (TextView)cv.findViewById(R.id.grade);
            TextView creditTxtV = (TextView)cv.findViewById(R.id.credit_hr);

            courseTxtV.setText(studentVm.getCourse_Name());
            gradeTxtV.setText(studentVm.getGrade());
            creditTxtV.setText(studentVm.getCredit_Hours());

            return cv;
        }
    }

    public static GradeListFragment newInstance(Student student, String id)
    {
        Bundle args = new Bundle();
        args.putParcelable(EXTA_STUDENT, student);
        args.putString(EXTRA_ID, id);

        GradeListFragment fragment = new GradeListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void updateUI(Student student)
    {
        if (student == null)
        {
            showProgressDialog(true);
            return;
        }
        showProgressDialog(false);
        ArrayList<StudentVm> studentVms = new ArrayList<>();
        ArrayList<Course> courses =  student.getCourses();
        for (Course course : courses) {
            StudentVm studentVm = new StudentVm(student.getmId(), course.getName(), Integer.valueOf(course.getCredit()).toString(), course.getGrade());
            studentVms.add(studentVm);
        }

        Log.d(TAG, "About to display data...");

        GradeListAdapter gradeAdapter = new GradeListAdapter(studentVms);
        setListAdapter(gradeAdapter);

    }

    private void showProgressDialog(boolean show)
    {
        if (show)
        {
            if (pDialog == null)
            {
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();

            }
            else{
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        else
        if (pDialog == null) return;
        pDialog.cancel();
    }


    private class StudentLoaderCallbacks implements LoaderManager.LoaderCallbacks<Student>{


        @Override
        public Loader<Student> onCreateLoader(int i, Bundle bundle) {
            return new StudentLoader(getActivity(), mId);
        }

        @Override
        public void onLoadFinished(Loader<Student> loader, Student student) {
            mStudent = student;
            Log.d(TAG, "Finished Loading student data...");
            updateUI(mStudent);
        }

        @Override
        public void onLoaderReset(Loader<Student> loader) {

        }
    }

    private class CoursesLoaderCallbacks implements LoaderManager.LoaderCallbacks<ArrayList<Course>>
    {

        @Override
        public Loader<ArrayList<Course>> onCreateLoader(int i, Bundle bundle) {
            return new CoursesLoader(getActivity(), _id);
        }

        @Override
        public void onLoadFinished(Loader<ArrayList<Course>> loader, ArrayList<Course> courses) {
            Log.d(TAG, "Finished Loading course data...");
           mStudent.setCourses(courses);
        }

        @Override
        public void onLoaderReset(Loader<ArrayList<Course>> loader) {

        }
    }
}
