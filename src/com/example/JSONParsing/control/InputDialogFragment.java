package com.example.JSONParsing.control;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.example.JSONParsing.R;

/**
 * Created by Xabush on 8/24/2015 10:33.
 * Project: JSONParsing
 */
public class InputDialogFragment extends DialogFragment {

    public static final String EXTRA_ID = "com.example.JSONParsing.control";
    private static final String TAG = "InputDialog";

    private String mId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstance)
    {
         View v = getActivity().getLayoutInflater().inflate(R.layout.id_input,null);
        String title = getArguments().getString("title");

        EditText editText = (EditText)v.findViewById(R.id.id_input);

        Log.d(TAG, "Dialog Created!");

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      mId = editText.getText().toString();
                        //TODO Add Regex to validate the ID Number
                        sendResult(Activity.RESULT_OK);
                    }
                })
                .create();
    }

    private void sendResult(int resultCode)
    {
        if (getTargetFragment() == null){
            MainFragment mainFragment = MainFragment.newInstance(mId);
            Log.d(TAG, "Sent " + mId + " to MainFragment");
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fragmentContainer,mainFragment);
            transaction.commit();
            return;
        }

        Intent i = new Intent();
        i.putExtra(EXTRA_ID, mId);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
    }

    public static InputDialogFragment newInstance(String title)
    {
        Bundle args = new Bundle();
        args.putString("title", title);
        InputDialogFragment fragment = new InputDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
