package com.example.JSONParsing.control;

import android.content.Context;
import com.example.JSONParsing.model.Course;
import com.example.JSONParsing.model.StudentDbAdapter;

import java.util.ArrayList;

/**
 * Created by Xabush on 9/12/2015 22:24.
 * Project: JSONParsing
 */
//Course Loader class
public class CoursesLoader extends DataLoader<ArrayList<Course>> {

    private long mId;
    private StudentDbAdapter dbAdapter;

    public CoursesLoader(Context context, long id)
    {
        super(context);
        mId = id;
        dbAdapter = new StudentDbAdapter(context);
    }

    @Override
    public ArrayList<Course> loadInBackground() {
        return dbAdapter.getCourses(mId);
    }
}
