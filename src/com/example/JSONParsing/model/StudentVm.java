package com.example.JSONParsing.model;

/**
 * Created by Xabush on 8/24/2015 09:38.
 * Project: JSONParsing
 */
public class StudentVm {

    private String Id;
    private String Course_Name;
    private String Credit_Hours;
    private String Grade;

    public StudentVm() {
    }

    public StudentVm(String id, String course_Name, String credit_Hours, String grade) {
        Id = id;
        Course_Name = course_Name;
        Credit_Hours = credit_Hours;
        Grade = grade;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCourse_Name() {
        return Course_Name;
    }

    public void setCourse_Name(String course_Name) {
        Course_Name = course_Name;
    }

    public String getCredit_Hours() {
        return Credit_Hours;
    }

    public void setCredit_Hours(String credit_Hours) {
        Credit_Hours = credit_Hours;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }
}
