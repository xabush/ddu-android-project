package com.example.JSONParsing.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Xabush on 8/20/2015 11:45.
 * Project: JSONParsing
 */
public class Student implements Parcelable {
    private long _id;
    private String mId;
    private String full_name;
    private String department;
    private int academic_year;
    private int year;

    private float total_gpa;
    private float prev_sem_gpa;
    private float sem_gpa;

    private ArrayList<Course> courses;

    public Student()
    {
        courses = new ArrayList<>();
    }

    public Student(String mId)
    {
        this.mId = mId;
        courses = new ArrayList<>();
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }


    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getAcademic_year() {
        return academic_year;
    }

    public void setAcademic_year(int academic_year) {
        this.academic_year = academic_year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<Course> courses) {
        this.courses = courses;
    }

    public float getTotal_gpa() {
        return total_gpa;
    }

    public void setTotal_gpa(float total_gpa) {
        this.total_gpa = total_gpa;
    }

    public float getPrev_sem_gpa() {
        return prev_sem_gpa;
    }

    public void setPrev_sem_gpa(float prev_sem_gpa) {
        this.prev_sem_gpa = prev_sem_gpa;
    }

    public float getSem_gpa() {
        return sem_gpa;
    }

    public void setSem_gpa(float sem_gpa) {
        this.sem_gpa = sem_gpa;
    }



    protected Student(Parcel in) {
        _id = in.readLong();
        mId = in.readString();
        full_name = in.readString();
        department = in.readString();
        academic_year = in.readInt();
        year = in.readInt();
        total_gpa = in.readFloat();
        prev_sem_gpa = in.readFloat();
        sem_gpa = in.readFloat();
        if (in.readByte() == 0x01) {
            courses = new ArrayList<Course>();
            in.readList(courses, Course.class.getClassLoader());
        } else {
            courses = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_id);
        dest.writeString(mId);
        dest.writeString(full_name);
        dest.writeString(department);
        dest.writeInt(academic_year);
        dest.writeInt(year);
        dest.writeFloat(total_gpa);
        dest.writeFloat(prev_sem_gpa);
        dest.writeFloat(sem_gpa);
        if (courses == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(courses);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Student> CREATOR = new Parcelable.Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }
}