package com.example.JSONParsing.model;

/**
 * Created by Xabush on 9/2/2015 15:51.
 * Project: JSONParsing
 */
public final class DbConstants {

    /**
     * Student table constants
     */
    public static final String STU_TABLE = "student";
    public static final String STU_COLUMN_ID = "id";
    public static final String COLUMN_FNAME = "full_name";
    public static final String COLUMN_DEPT = "department";
    public static final String COLUMN_ACYEAR = "academic_year";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_TOTALGPA = "total_gpa";
    public static final String COLUMN_SEMESTERGPA = "semester_gpa";
    public static final String COLUMN_PSEMGPA = "psem_gpa";

    public static final String S_CREATE_TABLE = "CREATE TABLE student (\n" +
            "_id integer primary key autoincrement , \n " +
            STU_COLUMN_ID + " varchar(100) unique    NOT NULL ,\n" +
            COLUMN_FNAME + " varchar(100)    ,\n" +
            COLUMN_DEPT + " varchar(100)    ,\n" +
            COLUMN_ACYEAR + " integer         ,\n" +
            COLUMN_YEAR + " integer               ,\n" +
            COLUMN_TOTALGPA + " float           ,\n" +
            COLUMN_SEMESTERGPA + " float         ,\n" +
            COLUMN_PSEMGPA + " float     \n"+
            ");";

    /**
     * Course table constants
     */
    public static final String COURSE_TABLE = "course";
    public static final String COURSE_COLUMN_ID = "cid";
    public static final String COLUMN_NAME = "cname";
    public static final String COLUMN_CREDIT = "credit_hr";
    public static final String COLUMN_GRADE = "grade";
    public static final String COLUMN_SEMESTER = "semester";
    public static final String COLUMN_SID = "s_id";

    public static final String C_CREATE_TABLE = "CREATE TABLE Course (\n" +
            "_id integer primary key autoincrement, " +
            COURSE_COLUMN_ID +" varchar(100)  NOT NULL ,\n" +
            COLUMN_NAME + " varchar(100)    NOT NULL     ,\n" +
            COLUMN_CREDIT +" integer    NOT NULL      ,\n" +
            COLUMN_GRADE + " varchar(2)    NOT NULL       ,\n" +
            COLUMN_SEMESTER + " integer    NOT NULL       ,\n" +
            COLUMN_SID +" integer    NOT NULL           ,\n" +
            "FOREIGN KEY (s_id) REFERENCES student(_id) );";

}
