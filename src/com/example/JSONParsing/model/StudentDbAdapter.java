package com.example.JSONParsing.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Xabush on 9/5/2015 22:43.
 * Project: JSONParsing
 */
public class StudentDbAdapter extends DbBaseAdapter {

    private static final String PREFS_TAG = "students";
    private static SharedPreferences mPrefs;
    private SQLiteDatabase db;

    public StudentDbAdapter(Context context)
    {
        super(context);
        db = openDb();
        mPrefs = context.getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
    }


    public  Student queryStudent(String id)
    {
        Student stu = new Student();
        Cursor cursor = db.query(DbConstants.STU_TABLE,
                null, // all columns
                DbConstants.STU_COLUMN_ID + "= ?", // look for a run ID
                new String[]{id}, // with this value
                null, // group by
                null, // order by
                null, // having
                "1"); // limit 1 row)

         cursor.moveToFirst();
        int _id = 0;
        while (!cursor.isAfterLast())
        {
            _id = cursor.getInt(cursor.getColumnIndex("_id"));
            stu.setmId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstants.STU_COLUMN_ID)));
            stu.setFull_name(cursor.getString(cursor.getColumnIndexOrThrow(DbConstants.COLUMN_FNAME)));
            stu.setAcademic_year(cursor.getInt(cursor.getColumnIndexOrThrow(DbConstants.COLUMN_ACYEAR)));
            stu.setDepartment(cursor.getString(cursor.getColumnIndexOrThrow(DbConstants.COLUMN_DEPT)));
            stu.setYear(cursor.getInt(cursor.getColumnIndexOrThrow(DbConstants.COLUMN_YEAR)));
        }

        cursor.close();
        stu.set_id(_id);
        return stu;
    }

    public ArrayList<Course> getCourses(long id)
    {
        ArrayList<Course> courses = new ArrayList<>();
        Cursor c =  db.query(DbConstants.COURSE_TABLE, null, DbConstants.COLUMN_SID + "= ?", new String[]{String.valueOf(id)}, null, null, null);

        c.moveToFirst();
        while (!c.isAfterLast())
        {
            Course course = new Course();
            course.setId(c.getString(c.getColumnIndex(DbConstants.COURSE_COLUMN_ID)));
            course.setName(c.getString(c.getColumnIndex(DbConstants.COLUMN_NAME)));
            course.setCredit(c.getInt(c.getColumnIndex(DbConstants.COLUMN_CREDIT)));
            course.setGrade(c.getString(c.getColumnIndex(DbConstants.COLUMN_GRADE)));
            course.setSemester(c.getString(c.getColumnIndex(DbConstants.COLUMN_SEMESTER)));

            courses.add(course);
        }

        c.close();
        db.close();

        return courses;
    }

    public boolean isStudentEmpty()
    {
        String count = "SELECT count(*) FROM student;";
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();

        int result = cursor.getInt(0);
        cursor.close();
        return result == 0;
    }


    public  long insertStudent(Student student)
    {
        ContentValues scv = new ContentValues();
        scv.put(DbConstants.STU_COLUMN_ID, student.getmId());
        scv.put(DbConstants.COLUMN_FNAME, student.getFull_name());
        scv.put(DbConstants.COLUMN_DEPT, student.getDepartment());
        scv.put(DbConstants.COLUMN_ACYEAR, student.getAcademic_year());
        scv.put(DbConstants.COLUMN_YEAR, student.getYear());
        scv.put(DbConstants.COLUMN_TOTALGPA, student.getTotal_gpa());
        scv.put(DbConstants.COLUMN_SEMESTERGPA, student.getSem_gpa());
        scv.put(DbConstants.COLUMN_PSEMGPA, student.getPrev_sem_gpa());

        long sid = db.insert(DbConstants.STU_TABLE, null, scv);

        for (Course c : student.getCourses())
        {
            ContentValues cv = new ContentValues();
            cv.put(DbConstants.COURSE_COLUMN_ID, c.getId());
            cv.put(DbConstants.COLUMN_NAME, c.getName());
            cv.put(DbConstants.COLUMN_CREDIT, c.getCredit());
            cv.put(DbConstants.COLUMN_GRADE, c.getGrade());
            cv.put(DbConstants.COLUMN_SEMESTER, c.getSemester());
            cv.put(DbConstants.COLUMN_SID, sid);

            db.insert(DbConstants.COURSE_TABLE, null, cv);
        }
        //save the student id locally for quick access
        mPrefs.edit().putString("sid",student.getmId()).apply();
        mPrefs.edit().putLong("_id", student.get_id()).apply();
        return sid;
    }
}
