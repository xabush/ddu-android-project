package com.example.JSONParsing.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Xabush on 8/20/2015 11:45.
 * Project: JSONParsing
 */
public class Course implements Parcelable {
    private String id;
    private String name;
    private String grade;
    private int credit;
    private String semester;

    public Course() {
    }

    public Course(String id, String name, String grade, int credit, String semester) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.credit = credit;
        this.semester = semester;
    }

    /**Constructor from Parcel,
     * reads back fields IN THE ORDER they were written
     * */
    public Course(Parcel pc)
    {
        id = pc.readString();
        name = pc.readString();
        grade = pc.readString();
        credit = pc.readInt();
        semester = pc.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    /** Used to give additional hints on how to process the received parcel.*/
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel pc, int flags) {
        pc.writeString(id);
        pc.writeString(name);
        pc.writeString(grade);
        pc.writeInt(credit);
        pc.writeString(semester);


    }

    /** Static field used to regenerate object, individually or as arrays */
    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>()
    {
        public Course createFromParcel(Parcel pc)
        {
            return new Course(pc);
        }

        public Course[] newArray(int size)
        {
            return new Course[size];
        }
    };
}
