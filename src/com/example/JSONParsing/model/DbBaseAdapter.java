package com.example.JSONParsing.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Xabush on 9/3/2015 15:52.
 * Project: JSONParsing
 */
public class DbBaseAdapter {

    protected static final String DATABASE_NAME = "studentdb.db";
    protected static final int DATABASE_VERSION = 1;
    protected static final String DROP_STATMENT = "DROP TABLE IF EXISTS " + DbConstants.COURSE_TABLE + "; DROP TABLE IF EXISTS " + DbConstants.STU_TABLE;

    protected static DbSQLiteHelper sqLiteHelper;
    protected static Context mContext;

    public DbBaseAdapter(Context context)
    {
         mContext = context;
    }



    protected  SQLiteDatabase openDb()
    {
        if (sqLiteHelper == null)
            sqLiteHelper = new DbSQLiteHelper(mContext);
        return sqLiteHelper.getWritableDatabase();
    }

    protected  void closeDb()
    {
        sqLiteHelper.close();
    }

    protected  class DbSQLiteHelper extends SQLiteOpenHelper
    {

        public DbSQLiteHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(DbConstants.S_CREATE_TABLE); //create student table
            db.execSQL(DbConstants.C_CREATE_TABLE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w("StudentDbHelper", "Updating db form v" + oldVersion + " to v" + newVersion);
            db.execSQL(DROP_STATMENT);
            onCreate(db);
        }
    }

}
